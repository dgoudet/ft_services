#!/bin/sh
kubectl delete -f .
kubectl delete -f ./mySQL
kubectl delete -f ./wordpress
kubectl delete -f ./phpmyadmin
kubectl delete -f ./nginx
kubectl delete -f ./ftps
kubectl delete -f ./influxdb
kubectl delete -f ./telegraf
kubectl delete -f ./grafana

