#!/bin/sh
rc boot
rc-service mariadb setup && rc-service mariadb start
mysql -u root --password=my_secret -e "CREATE DATABASE wordpress_db DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
mysql --user=root --password=my_secret  -e "GRANT ALL ON wordpress_db.* TO 'dgoudet'@'%' IDENTIFIED BY 'my_secret';"
mysql --user=root --password=my_secret  -e "FLUSH PRIVILEGES;"
