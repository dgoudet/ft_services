#!/bin/sh

#1. Start services: web-server, php
rc boot
rc-service nginx start
rc-service php-fpm7 start

#2. Create site, admin and users using wp-cli (command line interface with wordpress)
cd var/www/localhost/htdocs/wordpress
sleep 5
php wp-cli.phar core install --url="172.17.0.240:5050/wordpress" --title="Titre du Site" --admin_user="dgoudet" --admin_password="my_secret" --admin_email="goudet.daphne@gmail.com"
php wp-cli.phar user create leny goudet.leny@gmail.com --role=editor
php wp-cli.phar user create bere goudet.bere@gmail.com --role=author
