!/bin/sh
#start minikube
minikube start --vm-driver=docker
#install metallb
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.4/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.4/manifests/metallb.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
#Build docker images
eval $(minikube docker-env)
docker build -t dgoudet/mysql ./srcs/mySQL
docker build -t dgoudet/wordpress ./srcs/wordpress
docker build -t dgoudet/phpmyadmin ./srcs/phpmyadmin
docker build -t dgoudet/nginx ./srcs/nginx
docker build -t dgoudet/ftps ./srcs/ftps
docker build -t dgoudet/influxdb ./srcs/influxdb
docker build -t dgoudet/telegraf ./srcs/telegraf
docker build -t dgoudet/grafana ./srcs/grafana
#apply objects
kubectl apply -f ./srcs
kubectl apply -f ./srcs/mySQL
kubectl apply -f ./srcs/wordpress
kubectl apply -f ./srcs/phpmyadmin
kubectl apply -f ./srcs/nginx
kubectl apply -f ./srcs/ftps
kubectl apply -f ./srcs/influxdb
sleep 35
kubectl apply -f ./srcs/telegraf
kubectl apply -f ./srcs/grafana
#create kubectl dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
kubectl create serviceaccount dashboard-admin-sa
kubectl create clusterrolebinding dashboard-admin-sa --clusterrole=cluster-admin --serviceaccount=default:dashboard-admin-sa
#to access dashboard:
#Obtain token (kubectl describe secret)
#connect from localhost: kubectl proxy
#connect via internet: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
